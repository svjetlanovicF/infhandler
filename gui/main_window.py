import os
from PySide2.QtWidgets import QMainWindow, QMessageBox, QDockWidget
from PySide2.QtCore import Qt, QSettings
from PySide2.QtGui import QIcon
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.workspace.workspace_widget import WorkspaceWidget
from models.collection import Collection
from models.file import File
from models.workspace import Workspace

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("InfHandler")
        self.setWindowIcon(QIcon("resources/icons/logo.png"))
        self.resize(1000, 650)

        self.tool_bar = ToolBar(parent=self)
        self.menu_bar = MenuBar(self)
        self.central_widget = CentralWidget(parent=self)
        self.workspace_widget_file_system = WorkspaceWidget("File system", self)

        self.workspace_widget_file_system.setFeatures(QDockWidget.NoDockWidgetFeatures)

        self.setup_workspace()
        self.load_settings()

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.workspace_widget_file_system, Qt.Vertical)
        

    def save_settings(self):
        settings = QSettings("Company", "InfHandler")
        
        # Save parent tab information
        parent_tab_info = []
        for i in range(self.central_widget.parent_tabs_widget.count()):
            file_path = self.central_widget.get_file_path(i)
            parent_tab_info.append(file_path)
        
        # Save parent tab information to settings
        settings.setValue("parent_tab_info", parent_tab_info)
        
        # Save current tab index
        settings.setValue("current_tab_index", self.central_widget.parent_tabs_widget.currentIndex())

    def load_settings(self):
        settings = QSettings("Company", "InfHandler")
        
        # Load parent tab information
        parent_tab_info = settings.value("parent_tab_info", [])
        for parent_file_path in parent_tab_info:
            # Load parent tab
            if os.path.isfile(parent_file_path):
                selectedItem =  self.workspace_widget_file_system.get_workspace().workspace.search_file(parent_file_path)
                self.central_widget.load_file(selectedItem)

        # Set current tab index
        current_tab_index = settings.value("current_tab_index", 0)
        if current_tab_index < self.central_widget.parent_tabs_widget.count():
            self.central_widget.parent_tabs_widget.setCurrentIndex(current_tab_index)


    def closeEvent(self, event):
        self.save_settings()
        event.accept()

        # Prompt the user to confirm that they really want to exit the application
        confirm_exit = QMessageBox.question(
            self, "Confirm Exit", "Are you sure you want to exit?",
            QMessageBox.Yes | QMessageBox.No, QMessageBox.No
        )
        
        if confirm_exit == QMessageBox.Yes:
            # Call the default closeEvent method to clean up and exit the application
            super().closeEvent(event)
        else:
            # Ignore the close event and continue running the application
            event.ignore()

    def get_actions(self):
        return self.tool_bar.actions_dict

    def add_actions(self, where="menu", name=None, actions=[]):
        if where == "menu":
            self.menu_bar.add_actions(name, actions)
        elif where == "toolbar":
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)

    def setup_workspace(self):
        workspace = Workspace("Workspace", "./", [])
        nastavno_osoblje_collection = Collection('Nastavno osoblje', "resources/data/Nastavno osoblje", "resources/data/Nastavno osoblje_meta_file.json", workspace, [])
        studenti_collection = Collection('Studenti', "resources/data/Studenti", "resources/data/Studenti_meta_file.json", workspace, [])
        teritorijalna_organizacija_collection = Collection('Teritorijalna organizacija', "resources/data/TeritorijalnaOrganizacija", "resources/data/TeritorijalnaOrganizacija_meta_file.json", workspace, [])
        visokoskolska_ustanova_collection = Collection('Visokoskolska ustanova', "resources/data/Visokoskolska ustanova", "resources/data/Visokoskolska ustanova_meta_file.json", workspace, [])
        izvode_nastavu_file = File('Izvode nastavu', 'resources/data/Nastavno osoblje/izvode_nastavu.txt', 'resources/data/Nastavno osoblje/izvode_nastavu_meta_file.json', nastavno_osoblje_collection)
        nastavnici_file = File('Nastavnici', 'resources/data/Nastavno osoblje/nastavnici.txt', 'resources/data/Nastavno osoblje/nastavnici_meta_file.json', nastavno_osoblje_collection)
        istorija_polaganja_file = File('Istorija polaganja', "resources/data/Studenti/istorija_polaganja.txt", "resources/data/Studenti/istorija_polaganja_meta_file.json", studenti_collection)
        obaveze_studenata_file = File('Obaveze studenata', "resources/data/Studenti/obaveze_studenata.txt", "resources/data/Studenti/obaveze_studenata_meta_file.json", studenti_collection)
        studenti_file = File('Studenti', "resources/data/Studenti/studenti.txt", "resources/data/Studenti/studenti_meta_file.json", studenti_collection)
        tok_studija_file = File('Tok studija', "resources/data/Studenti/tok_studija.txt", "resources/data/Studenti/tok_studija_meta_file.json", studenti_collection)
        drzava_file = File('Drzava', "resources/data/TeritorijalnaOrganizacija/drzava.txt", "resources/data/TeritorijalnaOrganizacija/drzava_meta_file.json", teritorijalna_organizacija_collection)
        naseljeno_mesto_file = File('Naseljeno mesto', "resources/data/TeritorijalnaOrganizacija/naseljeno_mesto.txt", "resources/data/TeritorijalnaOrganizacija/naseljeno_mesto_meta_file.json", teritorijalna_organizacija_collection)
        sastav_drzave_file = File('Sastav drzave', "resources/data/TeritorijalnaOrganizacija/sastav_drzave.txt", "resources/data/TeritorijalnaOrganizacija/sastav_drzave_meta_file.json", teritorijalna_organizacija_collection)
        bazen_izbornih_file = File('Bazen izbornih', "resources/data/Visokoskolska ustanova/bazen_izbornih.txt", "resources/data/Visokoskolska ustanova/bazen_izbornih_meta_file.json", visokoskolska_ustanova_collection)
        nastavni_predmet_file = File('Nastavni predmet', "resources/data/Visokoskolska ustanova/nastavni_predmet.txt", "resources/data/Visokoskolska ustanova/nastavni_predmet_meta_file.json", visokoskolska_ustanova_collection)
        nivo_studija_file = File('Nivo studija', "resources/data/Visokoskolska ustanova/nivo_studija.txt", "resources/data/Visokoskolska ustanova/nivo_studija_meta_file.json", visokoskolska_ustanova_collection)
        plan_studijske_grupe_file = File('Plan studijske grupe', "resources/data/Visokoskolska ustanova/plan_studijske_grupe.txt", "resources/data/Visokoskolska ustanova/plan_studijske_grupe_meta_file.json", visokoskolska_ustanova_collection)
        struktura_ustanove_file = File('Struktura ustanove', "resources/data/Visokoskolska ustanova/struktura_ustanove.txt", "resources/data/Visokoskolska ustanova/struktura_ustanove_meta_file.json", visokoskolska_ustanova_collection)
        studijski_programi_file = File('Studijski programi', "resources/data/Visokoskolska ustanova/studijski_programi.txt", "resources/data/Visokoskolska ustanova/studijski_programi_meta_file.json", visokoskolska_ustanova_collection)
        visokoskolska_ustanova_file = File('Visokoskolska ustanova', "resources/data/Visokoskolska ustanova/visokoskolska_ustanova.txt", "resources/data/Visokoskolska ustanova/visokoskolska_ustanova_meta_file.json", visokoskolska_ustanova_collection)

        workspace.collections.append(nastavno_osoblje_collection)
        workspace.collections.append(studenti_collection)
        workspace.collections.append(teritorijalna_organizacija_collection)
        workspace.collections.append(visokoskolska_ustanova_collection)

        nastavno_osoblje_collection.add_child(izvode_nastavu_file)
        nastavno_osoblje_collection.add_child(nastavnici_file)

        studenti_collection.add_child(istorija_polaganja_file)
        studenti_collection.add_child(obaveze_studenata_file)
        studenti_collection.add_child(studenti_file)
        studenti_collection.add_child(tok_studija_file)

        teritorijalna_organizacija_collection.add_child(drzava_file)
        teritorijalna_organizacija_collection.add_child(naseljeno_mesto_file)
        teritorijalna_organizacija_collection.add_child(sastav_drzave_file)

        visokoskolska_ustanova_collection.add_child(bazen_izbornih_file)
        visokoskolska_ustanova_collection.add_child(nastavni_predmet_file)
        visokoskolska_ustanova_collection.add_child(nivo_studija_file)
        visokoskolska_ustanova_collection.add_child(plan_studijske_grupe_file)
        visokoskolska_ustanova_collection.add_child(struktura_ustanove_file)
        visokoskolska_ustanova_collection.add_child(studijski_programi_file)
        visokoskolska_ustanova_collection.add_child(visokoskolska_ustanova_file)

        self.workspace_widget_file_system.set_workspace(workspace)
