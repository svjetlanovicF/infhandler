import json
from models.collection import Collection
from PySide2.QtCore import QAbstractItemModel, QModelIndex, Qt
from models.file import File
from PySide2.QtGui import QIcon

from models.workspace import Workspace



class WorkspaceModel(QAbstractItemModel):
    def __init__(self, parent=None, workspace=None):
        super().__init__(parent)
        self.workspace = workspace


    def get_element(self, index: QModelIndex):
        if index.isValid():
            element = index.internalPointer()
            if element:
                return element
        return self.workspace    


    def rowCount(self, parent=QModelIndex()):
        item = self.get_element(parent)
        if item is not None:
            return len(item.get_children())
        return None

    def columnCount(self, parent=QModelIndex()):
        return 1 # Only one column for the item name
    
    def data(self, index, role=Qt.DisplayRole):
        item = self.get_element(index)

        if (index.column() == 0) and (role == Qt.DecorationRole):
            if isinstance(item, Collection):
                return QIcon("resources/icons/folder.png")
            elif isinstance(item, File):
                return QIcon("resources/icons/document.png")
        
        elif (index.column() == 0) and (role == Qt.DisplayRole):
            return item.name
        
        elif role == Qt.UserRole:
            return item
        
        return None


    def index(self, row, column, parent=QModelIndex()):
        item = self.get_element(parent)
        if ((row >= 0) and (row < len(item.get_children())) and item.get_children()[row]):
            return self.createIndex(row, column, item.get_children()[row])
        else:
            return QModelIndex()
        
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation != Qt.Vertical and role == Qt.DisplayRole:
            if section == 0:
                return "Name"
        
        return super().headerData(section, orientation, role)


    def parent(self, index):
        if index.isValid():
            child_element = self.get_element(index)
            parent_element = child_element.get_parent()
            if parent_element and (parent_element != self.workspace):
                row = parent_element.get_parent().get_children().index(parent_element)
                column = 0
                obj = parent_element
                return self.createIndex(row, column, obj)
        return QModelIndex()

    