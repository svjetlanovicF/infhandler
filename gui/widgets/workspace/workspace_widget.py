from PySide2.QtWidgets import QTreeView, QVBoxLayout, QDockWidget, QWidget, QHBoxLayout, QLabel
from PySide2.QtCore import QModelIndex
import sys

from gui.widgets.workspace.workspace_model import WorkspaceModel
from models.collection import Collection 

class WorkspaceWidget(QDockWidget):
    def __init__(self, title="Structure", parent=None):
        super().__init__(title, parent)
        self.structure_widget = QTreeView()
        self.structure_model = WorkspaceModel()
        self.structure_widget.setModel(self.structure_model)
        self.structure_widget.clicked.connect(self.handleItemClick)

        self.setWidget(self.structure_widget)

    def set_workspace(self, workspace):
        self.structure_model = WorkspaceModel(None, workspace)
        self.structure_widget.setModel(self.structure_model)

    def get_workspace(self):
        return self.structure_model

    def handleItemClick(self, index: QModelIndex):
        if index.isValid():
            selected_item = index.internalPointer()
            central_widget = self.parent().central_widget

            if isinstance(selected_item, Collection):
                self.structure_widget.setExpanded(index, not self.structure_widget.isExpanded(index))
            else:
                central_widget.loadData(selected_item)

