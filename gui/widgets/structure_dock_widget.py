from PySide2.QtWidgets import QDockWidget, QTreeView, QFileSystemModel, QMenu
from PySide2.QtCore import Qt, Signal

class StructureDockWidget(QDockWidget):
    open_file = Signal(str)

    def __init__(self, title="Structure", parent=None, root_path=None):
        super().__init__(title, parent)
        self.structure_model = QFileSystemModel()
        self.structure_widget = QTreeView()
        self.structure_widget.setModel(self.structure_model)
        self.structure_widget.setRootIndex(self.structure_model.index(root_path))
        self.structure_widget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.structure_widget.customContextMenuRequested.connect(self.show_context_menu)

        self.structure_model.setNameFilters(['*.txt'])
        self.structure_model.setNameFilterDisables(False)

        if root_path is not None:
            self.structure_model.setRootPath(root_path)
            self.structure_widget.setRootIndex(self.structure_model.index(root_path))
            
        self.structure_widget.hideColumn(1)
        self.structure_widget.hideColumn(2)
        self.structure_widget.hideColumn(3)
        
        self.setWidget(self.structure_widget)

        self.structure_widget.doubleClicked.connect(self.on_file_pressed)
        

    def on_file_pressed(self, index):
        file_path = self.structure_model.filePath(index)
        if file_path.endswith('.txt'):
            self.open_file.emit(file_path)

    def show_context_menu(self, pos):
        index = self.structure_widget.indexAt(pos)
        if index.isValid() and not self.structure_model.isDir(index):
            menu = QMenu(self.structure_widget)
            open_action = menu.addAction("Open")
            action = menu.exec_(self.structure_widget.mapToGlobal(pos))
            if action == open_action:
                self.on_file_pressed(index)

