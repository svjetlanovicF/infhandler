from PySide2.QtWidgets import QStatusBar, QPushButton, QDialog, QVBoxLayout, QLabel, QHBoxLayout, QLineEdit, QFormLayout
from PySide2.QtGui import QRegExpValidator
from PySide2.QtCore import Qt, QRegExp


class StatusBar(QStatusBar):
    def __init__(self, workspace, file="", parent=None):
        super().__init__(parent)
        self.file = file
        self.workspace = workspace
        self.create_button = QPushButton("Create")
        update_button = QPushButton("Update")
        delete_button = QPushButton("Delete")

        self.addWidget(self.create_button)
        self.addWidget(update_button)
        self.addWidget(delete_button)

        self.create_button.clicked.connect(self.show_create_dialog)

    def show_create_dialog(self):
        dialog = QDialog(self)
        dialog.setWindowTitle('Form')
        dialog.setWindowFlags(dialog.windowFlags() & ~Qt.WindowContextHelpButtonHint)  # Remove the question mark button
        layout = QVBoxLayout(dialog)
        # layout.addWidget(QLabel("Enter details for creating a new item:"))
        self.get_metadata(self.file, layout)
        # Add two buttons to the dialog
        submit_button = QPushButton("Submit")
        cancel_button = QPushButton("Cancel")
        button_layout = QHBoxLayout()
        button_layout.addWidget(submit_button)
        button_layout.addWidget(cancel_button)
        layout.addLayout(button_layout)

        

        # Connect slots or functions to handle the button actions
        submit_button.clicked.connect(self.handle_create)
        cancel_button.clicked.connect(dialog.reject)
        dialog.exec_()

    def get_metadata(self, file, layout):
        #get metafile path from workspace
        meta_file_content = self.workspace.get_content_from_meta_file(file)
        form_layout = QFormLayout()
        layout.addLayout(form_layout)
        for field in meta_file_content:
            label = QLabel(field['label'] + "*" if not field['isNullable'] else field['label'])
            input_field = QLineEdit()
            form_layout.addRow(label, input_field)

            if not field['isNullable']:
                input_field.setValidator(QRegExpValidator(QRegExp(".+")))
                input_field.setPlaceholderText("Required field")

    
    def handle_create(self):
        #TODO: handle create new row in table
        table_model = self.parentWidget().table.table_model
        input_fields = {}
        
        