from PySide2.QtWidgets import QMenuBar, QMenu


class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_menu = QMenu("File", self)
        self.database_menu = QMenu("Database", self)
        self.help_menu = QMenu("Help", self)
           
        self._populate_menus()

    def _populate_menus(self):
        actions = self.parent().get_actions()

        self.help_menu.addAction(actions["about"])
        self.file_menu.addAction(actions["newFile"])
        self.file_menu.addAction(actions["closeFile"])
        self.file_menu.addAction(actions["deleteFile"])
        self.database_menu.addAction(actions["openDatabase"])
        self.database_menu.addAction(actions["openTable"])

        self.addMenu(self.file_menu)
        self.addMenu(self.database_menu)
        self.addMenu(self.help_menu)

    def add_actions(self, menu="file", actions=[]):
        if len(actions) == 0:
            return
        if menu == "file":
            self.file_menu.addSeparator()
            self.file_menu.addActions(actions)
        elif menu == "edit":
            self.edit_menu.addSeparator()
            self.edit_menu.addActions(actions)
        elif menu == "help":
            self.help_menu.addSeparator()
            self.help_menu.addActions(actions)
        elif menu == "Database":
            self.database_menu.addSeparator()
            self.database_menu.addAction(actions)
