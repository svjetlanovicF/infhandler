from PySide2.QtWidgets import QDialog, QDialogButtonBox, QComboBox, QHBoxLayout, QVBoxLayout, QLabel, QFormLayout

class TableSelectionDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Select table from schema")
        self.resize(400, 200)

        self.dialog_layout = QVBoxLayout()

        self.table_layout = QFormLayout()
        self.tables_combobox = QComboBox()

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.table_layout.addRow('Table name:', self.tables_combobox)

        self.dialog_layout.addLayout(self.table_layout)
        self.dialog_layout.addStretch(1)
        self.dialog_layout.addWidget(self.button_box)

        self.setLayout(self.dialog_layout)

    def populate_tables(self, tables):
        self.tables_combobox.clear()
        self.tables_combobox.addItems(tables)

    def get_selected_table(self):
        return self.tables_combobox.currentText()
