from PySide2.QtWidgets import QDialog, QLineEdit, QDialogButtonBox, QComboBox, QFormLayout, QVBoxLayout, QMessageBox
from PySide2.QtGui import QIcon, Qt
from database.connection import open_connection


class DatabaseDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Connect to database")
        self.setWindowIcon(QIcon("resources/icons/database-storage.png"))
        self.resize(400, 200)

        self.dialog_layout = QVBoxLayout()
        self.form_layout = QFormLayout()

        self.database_name = QLineEdit()
        self.database_username = QLineEdit()
        self.database_password = QLineEdit()
        self.database_password.setEchoMode(QLineEdit.Password)

        self._populate_form_layout()

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.dialog_layout.addLayout(self.form_layout)
        self.dialog_layout.addWidget(self.button_box)

        # Remove question mark from the dialog
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)

        self.setLayout(self.dialog_layout)

    def _populate_form_layout(self):
        self.form_layout.addRow("Database name:", self.database_name)
        self.form_layout.addRow("Username:", self.database_username)
        self.form_layout.addRow("Password", self.database_password)

    def get_data(self):
        data = {
            "db_name": self.database_name.text(),
            "username": self.database_username.text(),
            "password": self.database_password.text()
        }
        return data

    def set_tables(self, tables):
        self.tables_combobox.clear()
        self.tables_combobox.addItems(tables)

    def get_selected_table(self):
        return self.tables_combobox.currentText()
