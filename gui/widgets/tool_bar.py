from PySide2.QtWidgets import QToolBar, QAction, QMessageBox,QDialog
from PySide2.QtGui import QIcon
from database.database import Database
from gui.widgets.database_dialog.database_dialog import DatabaseDialog
from database.connection import open_connection
from gui.widgets.database_dialog.table_selection_dialog import TableSelectionDialog


class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.actions_dict = {}
        self.default_actions()
        self.bind_actions()
        self.connection_data = None
        self.connection = None
        self.connected = False  # Flag to track database connection

    def default_actions(self):
        new_file_action = QAction(QIcon("resources/icons/new-document.png"), "New file")
        new_file_action.setStatusTip("Create new file")
        close_file_action = QAction(QIcon("resources/icons/close.png"), "Close file")
        close_file_action.setStatusTip("Close file")
        close_all_action = QAction(QIcon("resources/icons/close-all-files.png"), "Close all")
        close_all_action.setStatusTip("Close all files")
        delete_file_action = QAction(QIcon("resources/icons/delete.png"), "Delete file")
        delete_file_action.setStatusTip("Delete file")
        about_action = QAction(QIcon("resources/icons/about.png"), "About UI project")
        about_action.setStatusTip("About program")
        open_database_action = QAction(QIcon("resources/icons/database-storage.png"), "Open database")
        open_database_action.setStatusTip("Connect to the database")
        open_table_action = QAction(QIcon("resources/icons/category.png"), "Open table")
        open_table_action.setStatusTip("Open tables from the database")

        self.actions_dict["newFile"] = new_file_action
        self.actions_dict["closeFile"] = close_file_action
        self.actions_dict["closeAll"] = close_all_action
        self.actions_dict["deleteFile"] = delete_file_action
        self.actions_dict["about"] = about_action
        self.actions_dict["openDatabase"] = open_database_action
        self.actions_dict["openTable"] = open_table_action

        self.addAction(new_file_action)
        self.addAction(close_file_action)
        self.addAction(close_all_action)
        self.addAction(delete_file_action)
        self.addAction(about_action)
        self.addAction(open_database_action)
        self.addAction(open_table_action)

    def bind_actions(self):
        self.actions_dict["about"].triggered.connect(self.on_about_action)
        self.actions_dict["newFile"].triggered.connect(self.on_new_file_action)
        self.actions_dict["closeFile"].triggered.connect(self.on_close_file_action)
        self.actions_dict["closeAll"].triggered.connect(self.on_close_all_action)
        self.actions_dict["deleteFile"].triggered.connect(self.on_delete_file_action)
        self.actions_dict["openDatabase"].triggered.connect(self.on_open_database_action)
        self.actions_dict["openTable"].triggered.connect(self.on_open_table_action)

    def on_about_action(self):
        QMessageBox.information(self.parent(), "About program", "Project prototype", QMessageBox.Ok)

    def on_new_file_action(self):
        QMessageBox.information(self.parent(), "New", "Create new file", QMessageBox.Ok)

    def on_close_file_action(self):
        QMessageBox.information(self.parent(), "Close", "Close file", QMessageBox.Ok)

    def on_close_all_action(self):
        # QMessageBox.information(self.parent(), "Close all", "Close all files", QMessageBox.Ok)
        self.parent().central_widget.close_all()

    def on_delete_file_action(self):
        QMessageBox.information(self.parent(), "Delete", "Delete file", QMessageBox.Ok)

    def on_open_database_action(self):
        if not self.connected:
            database_dialog = DatabaseDialog(self)
            result = database_dialog.exec_()
            if result != QDialog.Accepted:
                return
            connection_data = database_dialog.get_data()
            connection = None

            if connection_data["username"] != '' and connection_data["password"] != '' and connection_data["db_name"] != '':
                try:
                    connection = open_connection(
                        "localhost",
                        connection_data["username"],
                        connection_data["password"],
                        connection_data["db_name"]
                    )
                except:
                    QMessageBox.warning(self.parent(), "Wrong credentials", "You need to provide the correct credentials!",
                                    QMessageBox.Ok)
                    return
            else:
                QMessageBox.warning(self.parent(), "Connection Error", "Unable to connect to the database.",
                                    QMessageBox.Ok)
                return

            if connection is not None:
                self.connection = connection
                self.connection_data = connection_data
                self.connected = True
                QMessageBox.information(self.parent(), "Database Connected", "Successfully connected to the database.",
                                        QMessageBox.Ok)
            else:
                QMessageBox.warning(self.parent(), "Connection Error", "Unable to connect to the database.",
                                    QMessageBox.Ok)
        else:
            QMessageBox.information(self.parent(), "Database Already Connected", "You are already connected to a database.",
                                    QMessageBox.Ok)


    def fetch_tables(self):
        try:
            cursor = self.connection.cursor()
            cursor.execute("SHOW TABLES")
            tables = [list(table.values())[0] for table in cursor.fetchall()]
            cursor.close()
            return tables
        except Exception as e:
            error_message = f"An error occurred while fetching tables: {str(e)}"
            QMessageBox.warning(self.parent(), "Error", error_message, QMessageBox.Ok)
            print(error_message)
            return []

    def on_open_table_action(self):
        if not self.connected:
            QMessageBox.warning(self.parent(), "Not Connected", "Please connect to a database first.", QMessageBox.Ok)
            return

        tables = self.fetch_tables()
        if tables:
            table_selection_dialog = TableSelectionDialog(self)
            table_selection_dialog.populate_tables(tables)

            result = table_selection_dialog.exec_()
            if result != QDialog.Accepted:
                return

            selected_table = table_selection_dialog.get_selected_table()

            # Fetch data from the selected table
            try:
                database = Database(connection=self.connection)
                column_names = database.get_column_names(selected_table)
                data = database.get_table_data(selected_table)
                self.parent().central_widget.load_table_from_database(data=data, column_names=column_names, table=selected_table)
            except Exception as e:
                error_message = f"An error occurred while fetching data from the table: {str(e)}"
                QMessageBox.warning(self.parent(), "Error", error_message, QMessageBox.Ok)
        else:
            QMessageBox.warning(self.parent(), "Error", "No tables found in the database.", QMessageBox.Ok)



