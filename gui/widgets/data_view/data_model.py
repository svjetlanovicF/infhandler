from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt



class DataModel(QAbstractTableModel):
    def __init__(self, parent=None, header_data=None, data=None):
        super().__init__(parent)
        self.table_data = data
        self.header_data = header_data

    def get_row(self, row=0):
        if self.table_data is not None:
            if row < len(self.table_data):
                data_row = self.table_data[row]
                if data_row:
                    return data_row
                
    def get_headers(self):
        return self.header_data
    
    def replace_data(self, row, data=[]):
        self.table_data[row] = data

    def get_element(self, index: QModelIndex):
        """
        :param index: indeks sa kojeg se dobavlja element (sadrzaj)
        :return: sadrzaj za datu celiju / cela lista (u slucaju da je indeks nevalidan)
        """
        if index.isValid():
            if 0 <= index.row() < len(self.table_data) and 0 <= index.column() < len(self.table_data[index.row()]):
                element = self.table_data[index.row()][index.column()]
                if element:
                    return element
        return ''

    def rowCount(self, parent=...):
        return len(self.table_data)
    
    def columnCount(self, parent=...):
        return len(self.header_data)
    
    def data(self, index, role=Qt.DisplayRole):
        element = self.get_element(index)
        if role == Qt.DisplayRole:
            return element
        
    def headerData(self, section: int, orientation:Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section]
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)
