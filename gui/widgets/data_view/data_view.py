import csv
import json
import os
from PySide2.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QTableView,
    QAction,
    QFileDialog,
    QAbstractItemView,
)
from PySide2.QtGui import QIcon
from PySide2.QtCore import Signal, Qt, QItemSelectionModel, QItemSelection
from gui.widgets.data_view.custom_item_delegate import CustomItemDelegate
from gui.widgets.data_view.data_model import DataModel


class DataView(QWidget):
    row_selected = Signal(list)
    def __init__(self, parent=None):
        super().__init__(parent)
        self.widget_layout = QVBoxLayout()
        self.table_model = None

        self.on_open_file_action = QAction(QIcon("resources/icons/folder.png"), "Open")
        self.on_open_file_action.triggered.connect(self.on_open_file_from_menu_and_toolbar)

        self.table_view = QTableView()
        self.table_view.setItemDelegate(CustomItemDelegate())
        self.table_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectRows)
        
        self.table_view.clicked.connect(self.on_row_selected)

        self.widget_layout.addWidget(self.table_view)
        self.setLayout(self.widget_layout)

    def on_row_selected(self, index):
        if index is not None:
            # Emit the row_selected signal with the selected row data
            selected_row = []
            for column in range(self.table_model.columnCount()):
                item = self.table_model.data(index.sibling(index.row(), column), Qt.DisplayRole)
                selected_row.append(item)
            self.row_selected.emit(selected_row)

    def read_header_data(self, meta_file_name):
        header = []
        with open(meta_file_name, "r") as f:
            meta_content = json.load(f)["content"]
            for column in meta_content:
                header.append(column["label"])
        return header

    def on_open_file_from_menu_and_toolbar(self):
        file_path = QFileDialog.getOpenFileName(
            self, "Open data", "resources/data/", "TXT files (*.txt)"
        )
        if file_path[0] == "":
            return

        self.load_data(file_path[0])

    def on_open_file_from_structure_dock(self, file_path):
        if file_path == "":
            return

        self.load_data(file_path)

    def load_data(self, file_path, metafile_path):
        header = self.read_header_data(metafile_path)

        with open(file_path, "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            data = list(reader)

            # Fill empty columns with ""
            for row in data:
                if len(row) < len(header):
                    row.extend([""] * (len(header) - len(row)))

            self.table_model = DataModel(data=data, header_data=header)
            self.table_view.setModel(self.table_model)

        self.table_view.resizeColumnsToContents()


    def load_relation_tables(self, data, metafile):
        header = []
        with open(metafile, "r") as f:
            meta_content = json.load(f)["content"]
            for column in meta_content:
                header.append(column["label"])
        self.table_model = DataModel(data=data, header_data=header)
        self.table_view.setModel(self.table_model)

        self.table_view.resizeColumnsToContents()

    def get_first_row_data(self):
        if self.table_model.rowCount() > 0:
            index = self.table_model.index(0, 0)
            first_row = QItemSelection(index, index.sibling(0, self.table_model.columnCount() - 1))
            self.table_view.selectionModel().select(first_row, QItemSelectionModel.Select)
            selected_row = []
            for column in range(self.table_model.columnCount()):
                item = self.table_model.data(index.sibling(0, column), Qt.DisplayRole)
                selected_row.append(item)
            return selected_row
        return None


    def export_actions(self):
        return [self.on_open_file_action]
