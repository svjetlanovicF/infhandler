import csv
import json
from collections import defaultdict
from PySide2.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QMessageBox
from gui.widgets.data_view.data_model import DataModel
from gui.widgets.data_view.data_view import DataView
from PySide2.QtCore import Slot, QModelIndex
from functools import partial
import json

from gui.widgets.status_bar import StatusBar


class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent_tabs_widget = QTabWidget()
        self.child_tabs_widget = QTabWidget()
        self.cw_layout = QVBoxLayout(self)
        self.table = None
        self.parent_tabs_widget.setTabsClosable(True)
        self.parent_tabs_widget.tabCloseRequested.connect(self.on_close_tab)
        self.parent_tabs_widget.currentChanged.connect(self.on_tab_changed)
        self.cw_layout.addWidget(self.parent_tabs_widget)
        self.cw_layout.addWidget(self.child_tabs_widget)
        self.setLayout(self.cw_layout)

        self.database_table = None

        self.open_files = {}  # keep track of opened files and their corresponding tabs
        self.open_child_files = defaultdict(dict)  # keep track of opened child files and their corresponding tabs
        self.all_files = {} 
        self.selected_row_data = None  # store the currently selected row data
        self.selected_child_tabs = defaultdict(int)  # store the currently selected child tab index for each parent tab
        self.tables_from_database = defaultdict(dict) #keep track of opened tables from database

    def load_table_from_database(self, data, column_names, table):
        for file in self.open_files:
            if table in file:
                QMessageBox.warning(self.parent(), "", "File or table is already opened!", QMessageBox.Ok)
                return

        self.database_table = DataView()
        tab = QWidget()
        tab_layout = QVBoxLayout(tab)
        tab_layout.addWidget(self.database_table)
        tab.setLayout(tab_layout)

        table_model = DataModel(header_data=column_names, data=data)
        self.database_table.table_view.setModel(table_model)

        self.tables_from_database[table] = tab

        self.parent_tabs_widget.addTab(tab, table)
        self.parent_tabs_widget.setCurrentWidget(tab)
        self.database_table.table_view.resizeColumnsToContents()


    #this method will be called when we switch the tab
    def on_tab_changed(self, index):
        self.child_tabs_widget.clear()
        file_path = self.get_file_path(index)
        children_files = self.all_files.get(file_path, {})

        for file_path, tab in children_files.items():
            self.child_tabs_widget.addTab(tab, file_path.split("/")[-1])

        # Restore the selected child tab index for the current parent tab
        self.child_tabs_widget.setCurrentIndex(self.selected_child_tabs[index])
        

    def loadData(self, selected_item):
        self.on_open_file(selected_item)


    def on_open_file(self, selected_item):
        file_path = selected_item.path
        metafile_path = selected_item.metadata_path
        # Check if the file is already opened
        if file_path in self.open_files:
            tab_index = self.parent_tabs_widget.indexOf(self.open_files[file_path])
            self.parent_tabs_widget.setCurrentIndex(tab_index)
            return
        
        #check if is table from database opened with same name
        for tableDb in self.tables_from_database:
            if tableDb in file_path:
                return

        # Create a new tab and add it to the tab widget
        tab = QWidget()
        tab_layout = QVBoxLayout(tab)
        table = DataView()
        self.table = table
        self.status_bar = StatusBar(file=file_path, workspace=self.get_current_workspace())
        tab_layout.addWidget(self.status_bar)
        tab_layout.addWidget(self.table)
        tab.setLayout(tab_layout)
        self.parent_tabs_widget.addTab(tab, file_path.split("/")[-1])

        # Load the file data into the table
        self.table.load_data(file_path, metafile_path)
        callback = partial(self.on_row_selected, selected_item=selected_item, file_path=file_path)
        self.table.row_selected.connect(callback)

        # Add the file and its corresponding tab to the dictionary
        self.open_files[file_path] = tab

        # Set focus to the newly opened tab
        self.parent_tabs_widget.setCurrentWidget(tab)

        # Call on_row_selected with the first row data
        self.on_row_selected(self.table.get_first_row_data(), selected_item, file_path)
        
        # Automatically select the first row and populate child data
        self.load_child_data_for_selected_row(selected_item, file_path)

        
    def load_child_data_for_selected_row(self, selected_item, file_path):
    # Get the first row data from the parent table
        row_data = self.table.get_first_row_data()
        if row_data:
            if not hasattr(self, "selected_row_data") or row_data != self.selected_row_data:
                self.selected_row_data = row_data
                self.on_update_children_tabs(row_data=row_data, selected_item=selected_item, file_path=file_path)
        
    @Slot(list)  # Decorator for the slot to match the row_selected signal
    def on_row_selected(self, row_data, selected_item, file_path):
        if not hasattr(self, "selected_row_data") or row_data != self.selected_row_data:
            self.selected_row_data = row_data
            self.on_update_children_tabs(row_data=row_data, selected_item=selected_item, file_path=file_path)

        
    def on_update_children_tabs(self, row_data, selected_item, file_path):
        metadata_path = selected_item.metadata_path
        with open(metadata_path, "r") as metafile:
            data = json.load(metafile)
            content = data['content']
            relations = data["relations"]
            if len(relations) == 0:
                return None

            parent_codes_values = {}
            for i in range(len(content)):
                if i < len(row_data):
                    parent_codes_values[content[i]['code']] = row_data[i]
                else:
                    break

            print('parent data: ', parent_codes_values)

            for relation in relations:
                child_file_path = relation['file']
                meta_file_path = self.get_current_workspace().get_meta_path(child_file_path)

                # Check if the child tab already exists
                if child_file_path in self.open_child_files[file_path]:
                    # If the tab exists, update the data inside the existing table
                    tab = self.open_child_files[file_path][child_file_path]
                    table = tab.findChild(DataView)
                    if table:
                        child_data = self.get_child_data(file=child_file_path, metafile=meta_file_path)
                        matching_rows = self.get_matching_rows(child_data, relation, parent_codes_values)
                        table.load_relation_tables(data=matching_rows, metafile=meta_file_path)
                        if len(matching_rows) > 0:
                            # Select the first row
                            first_row_index = table.table_model.index(0, 0, QModelIndex())
                            table.table_view.setCurrentIndex(first_row_index)
                else:
                    # If the tab doesn't exist, create a new tab and table
                    table = DataView()
                    tab = QWidget()
                    tab_layout = QVBoxLayout(tab)
                    tab_layout.addWidget(table)
                    tab.setLayout(tab_layout)
                    self.child_tabs_widget.addTab(tab, child_file_path.split("/")[-1])
                    self.open_child_files[file_path][child_file_path] = tab
                    
                    child_data = self.get_child_data(file=child_file_path, metafile=meta_file_path)
                    matching_rows = self.get_matching_rows(child_data, relation, parent_codes_values)
                    table.load_relation_tables(data=matching_rows, metafile=meta_file_path)
                    if len(matching_rows) > 0:
                            # Select the first row
                            first_row_index = table.table_model.index(0, 0, QModelIndex())
                            table.table_view.setCurrentIndex(first_row_index)

        self.selected_row_data = row_data  # Update the selected row data
        self.all_files[file_path] = self.open_child_files[file_path]

    # Store the selected child tab index for the current parent tab
        parent_tab_index = self.parent_tabs_widget.currentIndex()
        self.selected_child_tabs[parent_tab_index] = self.child_tabs_widget.currentIndex()

    def get_matching_rows(self, child_data, relation, parent_codes_values):
        matching_rows = []
        for row in child_data:
            if all(row.get(relation['foreignCode'][i]) == parent_codes_values.get(relation['code'][i]) for i in range(len(relation['code']))):
                matching_rows.append(list(row.values()))
        return matching_rows


    def get_child_data(self, file, metafile):
        child_raw_data = []
        child_labels = []
        
        with open(file, "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            data = list(reader)
            child_raw_data.extend(data)
        
        with open(metafile, "r") as mf:
            data = json.load(mf)
            child_labels.extend(data['content'])

        child_data = [{map_obj['code']: string for map_obj, string in zip(child_labels, child_raw_data)} for child_raw_data in child_raw_data]

        return child_data
             
    def on_close_tab(self, index):
        # Get the file path associated with the tab and remove it from the dictionary
        file_path = self.get_file_path(index)
        
        if file_path:
            # Remove the entry from open_files dictionary
            del self.open_files[file_path]
            
            # Clear the corresponding entries in open_child_files dictionary
            if file_path in self.open_child_files:
                del self.open_child_files[file_path]

            self.selected_row_data = None
        
        else:
            table_from_db = self.get_table_from_database(index)
            del self.tables_from_database[table_from_db]
                
        # Close the tab and remove it from the tab widget
        self.parent_tabs_widget.widget(index).deleteLater()
        self.parent_tabs_widget.removeTab(index)

    def get_file_path(self, index):
        # Get the file path associated with the tab at the given index
        if index >= 0:
            tab = self.parent_tabs_widget.widget(index)
            for file_path, tab_ in self.open_files.items():
                if tab_ == tab:
                    return file_path
        return None
    
    def get_table_from_database(self, index):
        if index >= 0:
            tab = self.parent_tabs_widget.widget(index)
            for table, tab_ in self.tables_from_database.items():
                if tab_ == tab:
                    return table
        return None
    

    def get_current_file_path(self):
        # Get the file path associated with the current tab
        current_index = self.parent_tabs_widget.currentIndex()
        return self.get_file_path(current_index)

    def load_file(self, file_path):
        # Open the file in a new tab
        self.on_open_file(file_path)

    def close_all(self):
        self.open_files = {} 
        self.open_child_files = defaultdict(dict)  
        self.all_files = {} 
        self.selected_row_data = None 
        self.selected_child_tabs = defaultdict(int)  
        self.tables_from_database = defaultdict(dict) 
        
        self.parent_tabs_widget.clear()

    def get_current_workspace(self):
        return self.parent().workspace_widget_file_system.structure_model.workspace

