from models.collection import Collection
from models.information_resource import InformationResource
import json


class Workspace(InformationResource):
    def __init__(self, name="", path="", collections=[]):
        super().__init__(name, path)
        self.collections = collections

    def get_parent():
        return None
    
    def get_children(self):
        return self.collections
    
    def handle(self):
        return None
    
    def add_child(self, item: Collection):
        self.collections.append(item)

    def get_meta_path(self, file_path):
        for collection in self.collections:
            for file in collection.content:
                if file.path == file_path:
                    return file.metadata_path
        return None
    
    def get_content_from_meta_file(self, file_path):
        meta_path = self.get_meta_path(file_path)
        with open(meta_path, 'r') as file:
            return json.load(file)["content"]
        
    def search_file(self, file_path):
        # Iterate over collections in the workspace
        for collection in self.collections:
            # Iterate over files in the collection
            for file in collection.content:
                # Check if the file path matches the search file path
                if file.path == file_path:
                    return file
        
        # Return None if the file is not found
        return None
