from abc import ABC, abstractmethod


class TypeHandler(ABC):
    @abstractmethod
    def handle(self):
        ...