from abc import ABC, abstractmethod

from models.interfaces.type_handler import TypeHandler

class InformationResource(TypeHandler, ABC):
    def __init__(self, name="", path=""):
        super().__init__()
        self.name = name
        self.path = path

    @abstractmethod
    def get_parent(): ...
    

    @abstractmethod
    def get_children(): ...

