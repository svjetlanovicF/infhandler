from models.information_resource import InformationResource


class File(InformationResource):
    def __init__(self, name="", path="", metadata_path="", parent=None):
        super().__init__(name, path)
        self.metadata_path = metadata_path
        self.parent = parent

    def get_parent(self):
        return self.parent
    
    def get_children(self):
        return []
    
    def handle(self):
        return None
    
    