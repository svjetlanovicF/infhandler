from models.file import File
from models.information_resource import InformationResource


class Collection(InformationResource):
    def __init__(self, name="", path="", metadata_path="", parent=None, content=[]):
        super().__init__(name, path)
        self.metadata_path = metadata_path
        self.parent = parent
        self.content = content

    def handle(self):
        return None

    def get_parent(self):
        return self.parent
    
    def get_children(self):
        return self.content
    
    def add_child(self, item: InformationResource):
        if len(self.content) == 0:
            self.content.append(item)
        elif isinstance(item, Collection) and all(isinstance(elem, Collection) for elem in self.content):
            self.content.append(item)
        elif isinstance(item, File) and all(isinstance(elem, File) for elem in self.content):
            self.content.append(item)
        else:
            return
            
        