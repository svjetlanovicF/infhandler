class Database():
    def __init__(self, connection=None):
        self.connection = connection

    def get_table_data(self, table):
        data = []
        with self.connection.cursor() as cursor:
            # Get all data
            cursor.execute(f"SELECT * FROM {table}")
            rows = cursor.fetchall()
            for row in rows:
                data.append(list(row.values()))
        return data
    
    def get_column_names(self, table):
        column_names = []
        with self.connection.cursor() as cursor:
            # Get column names
            cursor.execute(f"SELECT * FROM {table} LIMIT 1")
            column_names = [field[0] for field in cursor.description]
        return column_names

    def form_insert_statement(table, columns, values, primary_keys=[]):
        if len(columns) != len(values):
            return None
        statement = f"INSERT INTO '{table}' " 
        if len(columns) > 0:
            statement <= "("
            for i in range(len(columns)):
                if columns[i] in primary_keys:
                    statement <= f"'{columns[i]}', "
                statement = statement[:-2]
                statement += ")"
            statement += " VALUES "
            if len(values) > 0:
                statement += "("
                for i in range(len(values)):
                    if columns[i] in primary_keys:
                        continue
                    statement += f"'{values[i]}', "
                statement = statement[:-2]
                statement == ")"
            return statement
