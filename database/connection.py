from pymysql import connect, cursors


def open_connection(host="", user="", password="", database=""):
    connection = connect(host=host,
                        user=user,
                        password=password,
                        database=database,
                        charset="utf8mb4",
                        cursorclass=cursors.DictCursor)
    return connection


